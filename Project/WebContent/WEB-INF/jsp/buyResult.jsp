<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入完了</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/hover-min.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.10.0/css/all.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<header>
		<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-dark border-bottom shadow-sm">
			<a class="my-0 mr-md-auto font-weight-normal btn btn-outline-light" href="TopServlet"><i class="fas fa-home fa-lg" style="margin-right:5px"></i>夢の車屋さん</a>
				<nav class="my-2 my-md-0 mr-md-3">
					<a class="btn btn-outline-light" href="CarSearchServlet?page=1">在庫車一覧</a>
					<a class="btn btn-outline-light"style="margin:10px" href="UserAddServlet">新規登録</a>
					<a class="btn btn-outline-light" href="FavoSearchServlet?user_id=${sessionScope.userInfo.user_id}"><i class="fas fa-star"></i>Favorite</a>
					<a class="btn btn-outline-light" style="margin:10px"href="LoginServlet"><i class="fas fa-file-signature"></i>Sign up</a>
					<a class="btn btn-outline-light" href="LogoutServlet"><i class="fas fa-sign-out-alt"></i>Log out</a>
					<a class="text-white" style="margin-left:5px">Hello!! ${sessionScope.userInfo.name}さん</a>
				</nav>
		</div>
	</header>
	<div class="container">
		<h1 class="text-center" style="margin:30px"><i class="fas fa-child icon-blue"></i> ご購入ありがとうございます！！<i class="fas fa-child icon-blue"></i></h1>
		<h4 class="text-center" style="margin:50px">納車時期につきましては、追って連絡致します。</h4>
		<h4 class="text-center" style="margin:20px">楽しみにお待ち下さい！！</h4>
			<div class="text-center" style="margin:10px">
				<img src="img/${car.image}" width="400" height="280" class="border_radius" style="margin:10px">
			</div>
			<ul class="list-group mb-3">
				<li class="list-group-item d-flex justify-content-between lh-condensed">
					<div><h3 class="my-0">車体価格　(諸費用込み)</h3>
					</div> <h3>${car.price}円</h3>					</li>
				<li class="list-group-item d-flex justify-content-between lh-condensed">
					<div>
						<h3 class="my-0">${op.menu}</h3>
					</div>
						<h3>${op.price}円</h3>
				</li>
					<li class="list-group-item d-flex justify-content-between"><h3>お支払い金額</h3>
						<h3>${op.price+car.price}円</h3>
					</li>
			</ul>
			<div class="row">
				<div class="col-6" align="center">
					<a class="btn btn-outline-secondary" href="TopServlet">トップ画面へ</a>
				</div>
				<div class="col-6" align="center">
					<a class="btn btn-outline-secondary" href="FavoSearchServlet?user_id=${sessionScope.userInfo.user_id}">お気に入りへ</a>
				</div>
			</div>
	</div>
</body>
<footer class="footer mt-auto py-3">
  <div class="container">
    <h3 class="text-center icon-orange">Thank you for buying it !!</h3>
  </div>
 </footer>
</html>