<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>在庫車　新規登録</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/hover-min.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.10.0/css/all.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<header>
		<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-dark border-bottom shadow-sm">
			<a class="my-0 mr-md-auto font-weight-normal btn btn-outline-light" href="TopServlet"><i class="fas fa-home fa-lg" style="margin-right:5px"></i>夢の車屋さん</a>
				<nav class="my-2 my-md-0 mr-md-3">
					<a class="btn btn-outline-light" href="CarSearchServlet?page=1">在庫車一覧</a>
					<a class="btn btn-outline-light"style="margin:10px" href="UserAddServlet">新規登録</a>
					<a class="btn btn-outline-light" href="FavoSearchServlet?user_id=${sessionScope.userInfo.user_id}"><i class="fas fa-star"></i>Favorite</a>
					<a class="btn btn-outline-light" style="margin:10px"href="LoginServlet"><i class="fas fa-file-signature"></i>Sign up</a>
					<a class="btn btn-outline-light" href="LogoutServlet"><i class="fas fa-sign-out-alt"></i>Log out</a>
					<a class="text-white" style="margin-left:5px">Hello!! ${sessionScope.userInfo.name}さん</a>
				</nav>
		</div>
	</header>
	<div class="container">

		<div class="text-center" style="margin:30px">
			<h1>車両　新規登録</h1>
		</div>

		<form action="CarAddServlet" method="post">
			<div class="form-group">
				<div class="row">
					<div class="col-4 text-right">メーカー名</div>
					<div class="col-8">
						<input type="text" name="maker" class="form-control" id="maker" placeholder="メーカー名" style="width:400px">
					</div>
				</div>
			</div>
			<div class="form-group mt-5">
				<div class="row">
					<div class="col-4 text-right">車名</div>
					<div class="col-8">
						<input type="text" name="name" class="form-control" id="name" placeholder="車名" style="width:400px">
					</div>
				</div>
			</div>
			<div class="form-group mt-5">
				<div class="row">
					<div class="col-4 text-right">値段</div>
					<div class="col-8">
						<input type="text" class="form-control" id="passwordConfirm" name="price" placeholder="値段" style="width:400px">
					</div>
				</div>
			</div>
			<div class="form-group mt-5">
				<div class="row">
					<div class="col-4 text-right">
						年式
					</div>
					<div class="col-8">
						<input type="text" name="year" class="form-control" id="year" placeholder="年式" style="width:400px">
					</div>
				</div>
			</div>
			<div class="form-group mt-5">
				<div class="row">
					<div class="col-4 text-right">紹介文</div>
					<div class="col-8">
						<textarea  rows="20" cols="50" id="passwordConfirm" name="detail" style="width:400px;height:300px"></textarea>
					</div>
				</div>
			</div>
			<div class="form-group mt-5">
				<div class="row">
					<div class="col-sm-12 text-center">
						<label for="exampleFormControlFile1">車の画像を選択してください</label>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 text-center">
						<input type="file" id="exampleFormControlFile1" name="image">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 text-center">
					<button type="submit" class="btn btn-secondary ">登録</button>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 text-left">
					<a class="btn btn-outline-secondary " href="CarAdminServlet">Back</a>
				</div>
			</div>
		</form>
	</div>
<footer class="footer mt-auto py-3">
  <div class="container">
  	<div class="row">
  		<div class="col-6">
   			<span class="text-muted">made by WA TA SHI</span>
   		</div>
   		<div class="col-6 text-right">
   			<span class="text-muted"><a href="LoginAdmin">管理画面</a></span>
   		</div>
    </div>
  </div>
</footer>
</body>
</html>