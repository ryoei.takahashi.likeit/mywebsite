<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>管理画面</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/hover-min.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.10.0/css/all.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<header>
		<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-dark border-bottom shadow-sm">
			<a class="my-0 mr-md-auto font-weight-normal btn btn-outline-light" href="TopServlet"><i class="fas fa-home fa-lg" style="margin-right:5px"></i>夢の車屋さん</a>
				<nav class="my-2 my-md-0 mr-md-3">
					<a class="btn btn-outline-light" href="CarSearchServlet?page=1">在庫車一覧</a>
					<a class="btn btn-outline-light"style="margin:10px" href="UserAddServlet">新規登録</a>
					<a class="btn btn-outline-light" href="FavoSearchServlet?user_id=${sessionScope.userInfo.user_id}"><i class="fas fa-star"></i>Favorite</a>
					<a class="btn btn-outline-light" style="margin:10px"href="LoginServlet"><i class="fas fa-file-signature"></i>Sign up</a>
					<a class="btn btn-outline-light" href="LogoutServlet"><i class="fas fa-sign-out-alt"></i>Log out</a>
					<a class="text-white" style="margin-left:5px">Hello!! ${sessionScope.userInfo.name}さん</a>
				</nav>
		</div>
	</header>
	<div class="container">
		<h1 class="text-center" style="margin-top:20px;margin-bottom:20px">販売履歴　詳細</h1>
		<div class="text-center">
			<table class="table table-bordered">
				    <tr>
  						<th scope="row">購入者</th>
   						<td>${History.user_name}</td>
  					</tr>
  					<tr>
  						<th scope="row">車種</th>
   						<td>${History.car_name}</td>
  					</tr>
  					<tr>
  						<th scope="row">メーカー</th>
   						<td>${History.car_maker}</td>
  					</tr>
  					<tr>
  						<th scope="row">車体価格</th>
   						<td>${History.price}</td>
  					</tr>
  					<tr>
  						<th scope="row">オプション</th>
   						<td>${History.menu}</td>
  					</tr>
  					<tr>
  						<th scope="row">オプション価格</th>
   						<td>${History.op_price}</td>
  					</tr>
  					<tr>
  						<th scope="row">合計金額</th>
   						<td>${History.total_price}</td>
  					</tr>
  					<tr>
  						<th scope="row">販売日</th>
   						<td>${History.create_date}</td>
  					</tr>
			</table>
			<div class="row">
				<div class="col-sm-12 text-left">
					<a class="btn btn-outline-secondary " href="BuyHistory">Back</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>