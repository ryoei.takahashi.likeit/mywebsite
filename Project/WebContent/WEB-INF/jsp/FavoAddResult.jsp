<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Good Choice!!</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/hover-min.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.10.0/css/all.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<header>
		<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-dark border-bottom shadow-sm">
			<a class="my-0 mr-md-auto font-weight-normal btn btn-outline-light" href="TopServlet"><i class="fas fa-home fa-lg" style="margin-right:5px"></i>夢の車屋さん</a>
				<nav class="my-2 my-md-0 mr-md-3">
					<a class="btn btn-outline-light" href="CarSearchServlet?page=1">在庫車一覧</a>
					<a class="btn btn-outline-light"style="margin:10px" href="UserAddServlet">新規登録</a>
					<a class="btn btn-outline-light" href="FavoSearchServlet?user_id=${sessionScope.userInfo.user_id}"><i class="fas fa-star"></i>Favorite</a>
					<a class="btn btn-outline-light" style="margin:10px"href="LoginServlet"><i class="fas fa-file-signature"></i>Sign up</a>
					<a class="btn btn-outline-light" href="LogoutServlet"><i class="fas fa-sign-out-alt"></i>Log out</a>
					<a class="text-white" style="margin-left:5px">Hello!! ${sessionScope.userInfo.name}さん</a>
				</nav>
		</div>
	</header>
	<div class="container">
		<h1 class="text-center" style="margin:30px"><i class="fas fa-star icon-gold"></i>Good Choice<i class="fas fa-star icon-gold"></i></h1>
		<h3 class="text-center">引き続き、車選びをお楽しみください。</h3>
		<div class="row" style="margin-top:30px">
			<div class="col-4 text-right">
				<a class="btn btn-outline-secondary" href="FavoSearchServlet?user_id=${sessionScope.userInfo.user_id}" style="width:160px">お気に入り一覧</a>
			</div>
			<div class="col-4 text-center">
				<a class="btn btn-outline-secondary" href="CarDetailServlet?car_id=${car.car_id}" style="width:160px">詳細情報へ戻る</a>
			</div>
			<div class="col-4 text-left">
				<a class="btn btn-outline-secondary" href="CarSearchServlet" style="width:160px">在庫一覧</a>
			</div>
		</div>
		<div class="row">
			<div class="col-7">
				<img src="img/${car.image}" width="600" height="450" class="border_radius" style="margin:20px">
			</div>
			<div class="col-5">
				<div class="card" style="width:420px;height:450px;margin:20px">
					<div class="card-body">
						<h2 class="card-title">・車名<br>  ${car.name}</h2>
						<h2 class="card-subtitle mb-2 " style="margin-top:20px">・メーカー<br>  ${car.maker}</h2>
						<h2 class="card-text" style="margin-top:20px">・年式<br>${car.year}</h2>
						<br>
						<h3 class="card-text">・車体価格<br>  ￥${car.price}</h3>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<a class="button btn btn-primary btn-lg text-center" style="width:130px" href="BuyServlet?car_id=${car.car_id}"><i class="far fa-handshake" style="margin-right:5px"></i>Buy it!</a>
			</div>
		</div>
	</div>
<footer class="footer mt-auto py-3">
  <div class="container">
  	<div class="row">
  		<div class="col-6">
   			<span class="text-muted">made by WA TA SHI</span>
   		</div>
   		<div class="col-6 text-right">
   			<span class="text-muted"><a href="LoginAdmin">管理画面</a></span>
   		</div>
    </div>
  </div>
</footer>
</body>
</html>