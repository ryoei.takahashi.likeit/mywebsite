<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー管理</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/hover-min.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.10.0/css/all.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<header>
		<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-dark border-bottom shadow-sm">
			<a class="my-0 mr-md-auto font-weight-normal btn btn-outline-light" href="TopServlet"><i class="fas fa-home fa-lg" style="margin-right:5px"></i>夢の車屋さん</a>
				<nav class="my-2 my-md-0 mr-md-3">
					<a class="btn btn-outline-light" href="CarSearchServlet?page=1">在庫車一覧</a>
					<a class="btn btn-outline-light"style="margin:10px" href="UserAddServlet">新規登録</a>
					<a class="btn btn-outline-light" href="FavoSearchServlet?user_id=${sessionScope.userInfo.user_id}"><i class="fas fa-star"></i>Favorite</a>
					<a class="btn btn-outline-light" style="margin:10px"href="LoginServlet"><i class="fas fa-file-signature"></i>Sign up</a>
					<a class="btn btn-outline-light" href="LogoutServlet"><i class="fas fa-sign-out-alt"></i>Log out</a>
					<a class="text-white" style="margin-left:5px">Hello!! ${sessionScope.userInfo.name}さん</a>
				</nav>
		</div>
	</header>
	<div class="container">
		<div class="text-center" style="margin:30px">
			<h2>ユーザ管理画面</h2>
		</div>
			<form action="UserAdminServlet" method="post">
			<div class="row">

				<div class="col-4 text-right">ログインID</div>
				<div class="col-8">
					<div class="form-group">
						<input type="text" class="form-control" id="login_id"
							placeholder="ログインID" name="login_id" style="width:445px">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-4 text-right">ユーザ名</div>
				<div class="col-8">
					<div class="form-group">
						<input type="text" class="form-control"
							id="name" placeholder="ユーザ名" name="name" style="width:445px">
					</div>
				</div>
			</div>

			<div class="row">

				<div class="col-4 text-right">生年月日</div>

				<div class="col-2">
					<div class="form-group">
						<input type="date" class="form-control" id="stBirthday"
							placeholder="生年月日" name="stBirth_date">
					</div>
				</div>

				<div class="col-1 text-center">
					<h2>～</h2>
				</div>

				<div class="col-2">
					<div class="form-group">
						<input type="date" class="form-control" id="edBirthday"
							placeholder="生年月日" name="edBirth_date">
					</div>
				</div>

			</div>
			<div align="center">
				<button type="submit" class="btn btn-primary">検索</button>
			</div>
		</form>
		<div class="text-left" style="margin:30px">
			<a class="btn btn-outline-secondary" href="adminServlet">Back</a>
		</div>
		<table class="table" style="margin:20px">


			<thead class="table-bordered">
				<tr>
					<th scope="col">ログインID</th>
					<th scope="col">ユーザ名</th>
					<th scope="col">生年月日</th>
					<th scope="col"></th>
				</tr>

			</thead>
			<tbody>
			 <c:forEach var="list" items="${userList}">
				<tr>
					<th scope="row">${list.login_id}</th>
					<td>${list.name}</td>
					<td>${list.birth_date}</td>
                     <td>
                       <a class="btn btn-primary" href="UserDetailServlet?user_id=${list.user_id}">詳細</a>
                       	<a class="btn btn-success" href="UserUpdateServlet?user_id=${list.user_id}">更新</a>
                       <a class="btn btn-danger" href ="UserDeleteServlet?user_id=${list.user_id}">削除</a>
                     </td>
				</tr>
			</c:forEach>
				<tr>
			</tbody>
		</table>
	</div>
<footer class="footer mt-auto py-3">
  <div class="container">
  	<div class="row">
  		<div class="col-6">
   			<span class="text-muted">made by WA TA SHI</span>
   		</div>
   		<div class="col-6 text-right">
   			<span class="text-muted"><a href="LoginAdmin">管理画面</a></span>
   		</div>
    </div>
  </div>
</footer>
</body>
</html>