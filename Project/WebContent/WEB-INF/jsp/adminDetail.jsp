<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>在庫車　詳細</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/hover-min.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.10.0/css/all.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<header>
		<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-dark border-bottom shadow-sm">
			<a class="my-0 mr-md-auto font-weight-normal btn btn-outline-light" href="TopServlet"><i class="fas fa-home fa-lg" style="margin-right:5px"></i>夢の車屋さん</a>
				<nav class="my-2 my-md-0 mr-md-3">
					<a class="btn btn-outline-light" href="CarSearchServlet?page=1">在庫車一覧</a>
					<a class="btn btn-outline-light"style="margin:10px" href="UserAddServlet">新規登録</a>
					<a class="btn btn-outline-light" href="FavoSearchServlet?user_id=${sessionScope.userInfo.user_id}"><i class="fas fa-star"></i>Favorite</a>
					<a class="btn btn-outline-light" style="margin:10px"href="LoginServlet"><i class="fas fa-file-signature"></i>Sign up</a>
					<a class="btn btn-outline-light" href="LogoutServlet"><i class="fas fa-sign-out-alt"></i>Log out</a>
					<a class="text-white" style="margin-left:5px">Hello!! ${sessionScope.userInfo.name}さん</a>
				</nav>
		</div>
	</header>
	<div class="container">
		<h1 class="text-center">在庫車　詳細情報</h1>
			<div class="col-sm-12 text-center">
				<img src="img/${car.image}" width="300" height="200"  style="margin:20px" class="border_radius">
			</div>
		<div class="text-center">
			<ul class="list-group mb-3">
				<li class="list-group-item justify-content-between lh-condensed text-center">
					<h4>車名：${car.name}</h4>
				</li>
				<li class="list-group-item justify-content-between lh-condensed text-center">
					<h4>メーカー：${car.maker}</h4>
				</li>
				<li class="list-group-item justify-content-between lh-condensed text-center">
					<h4>年式：${car.year}</h4>
				</li>
				<li class="list-group-item justify-content-between lh-condensed d-flex">
					<h4 class="my-0">${car.detail}</h4>
				</li>
				<li class="list-group-item justify-content-between text-center">
					<h4>￥${car.price}</h4>
				</li>
				<li class="list-group-item justify-content-between lh-condensed text-center">
					<h4>登録日：${car.create_date}</h4>
				</li>
				<li class="list-group-item justify-content-between lh-condensed text-center">
					<h4>更新日：${car.update_date}</h4>
				</li>
			</ul>
			<div class="row">
				<div class="col-sm-12 text-left">
					<a class="btn btn-outline-secondary " href="CarAdminServlet">Back</a>
			</div>
			</div>
		</div>
	</div>
<footer class="footer mt-auto py-3">
  <div class="container">
  	<div class="row">
  		<div class="col-6">
   			<span class="text-muted">made by WA TA SHI</span>
   		</div>
   		<div class="col-6 text-right">
   			<span class="text-muted"><a href="LoginAdmin">管理画面</a></span>
   		</div>
    </div>
  </div>
</footer>
</body>
</html>