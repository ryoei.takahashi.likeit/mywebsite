<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報更新</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/hover-min.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.10.0/css/all.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<header>
		<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-dark border-bottom shadow-sm">
			<a class="my-0 mr-md-auto font-weight-normal btn btn-outline-light" href="TopServlet"><i class="fas fa-home fa-lg" style="margin-right:5px"></i>夢の車屋さん</a>
				<nav class="my-2 my-md-0 mr-md-3">
					<a class="btn btn-outline-light" href="CarSearchServlet?page=1">在庫車一覧</a>
					<a class="btn btn-outline-light"style="margin:10px" href="UserAddServlet">新規登録</a>
					<a class="btn btn-outline-light" href="FavoSearchServlet?user_id=${sessionScope.userInfo.user_id}"><i class="fas fa-star"></i>Favorite</a>
					<a class="btn btn-outline-light" style="margin:10px"href="LoginServlet"><i class="fas fa-file-signature"></i>Sign up</a>
					<a class="btn btn-outline-light" href="LogoutServlet"><i class="fas fa-sign-out-alt"></i>Log out</a>
					<a class="text-white" style="margin-left:5px">Hello!! ${sessionScope.userInfo.name}さん</a>
				</nav>
		</div>
	</header>
	<div class="container">
			<h1 class="text-center" style="margin:30px">登録情報更新</h1>
			<c:if test="${errMsg != null}" >
	    		<div class="alert alert-danger" role="alert">
		  		${errMsg}
				</div>
			</c:if>
			<div class="form-group">
				<div class="row">
					<div class="col-4 text-right">
						ログインID
					</div>
					<div class="col-8">
						<h4>${user.login_id}</h4>
					</div>
				</div>
			</div>
			<form action="UserUpdateServlet" method="post">
			<input type="hidden" name="user_id" value="${user.user_id}">
			<input type="hidden" name="login_id" value="${user.login_id}">
				<div class="form-group mt-5">
					<div class="row">
						<div class="col-4 text-right">
							ユーザ名
						</div>
						<div class="col-8">
							<input type="text" name="name" class="form-control" id="name" value="${user.name}" style="width:400px">
						</div>
					</div>
				</div>
				<div class="form-group mt-5">
					<div class="row">
						<div class="col-4 text-right">パスワード</div>
						<div class="col-8">
							<input type="password" class="form-control" id="passwordConfirm" name="password" style="width:400px">
						</div>
					</div>
				</div>
				<div class="form-group mt-5">
					<div class="row">
						<div class="col-4 text-right">パスワード(確認)</div>
						<div class="col-8">
							<input type="password" class="form-control" id="passwordConfirm" name="password2" style="width:400px">
						</div>
					</div>
				</div>
				<div class="form-group mt-5">
					<div class="row">
						<div class="col-4 text-right">
							生年月日
						</div>
						<div class="col-8">
							<input type="date" name="birth_date" class="form-control" id="birthDate" value="${user.birth_date}" style="width:400px">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 text-center">
						<button type="submit" class="btn btn-secondary ">更新</button>
					</div>
				</div>
			</form>
			<div class="row">
				<div class="col-sm-12 text-left">
					<a class="btn btn-outline-secondary " href="UserAdminServlet">Back</a>
				</div>
			</div>
	</div>
<footer class="footer mt-auto py-3">
  <div class="container">
  	<div class="row">
  		<div class="col-6">
   			<span class="text-muted">made by WA TA SHI</span>
   		</div>
   		<div class="col-6 text-right">
   			<span class="text-muted"><a href="LoginAdmin">管理画面</a></span>
   		</div>
    </div>
  </div>
</footer>
</body>
</html>