<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>詳細情報</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/hover-min.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.10.0/css/all.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<header>
		<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-dark border-bottom shadow-sm">
			<a class="my-0 mr-md-auto font-weight-normal btn btn-outline-light" href="TopServlet"><i class="fas fa-home fa-lg" style="margin-right:5px"></i>夢の車屋さん</a>
				<nav class="my-2 my-md-0 mr-md-3">
					<a class="btn btn-outline-light" href="CarSearchServlet?page=1">在庫車一覧</a>
					<a class="btn btn-outline-light"style="margin:10px" href="UserAddServlet">新規登録</a>
					<a class="btn btn-outline-light" href="FavoSearchServlet?user_id=${sessionScope.userInfo.user_id}"><i class="fas fa-star"></i>Favorite</a>
					<a class="btn btn-outline-light" style="margin:10px"href="LoginServlet"><i class="fas fa-file-signature"></i>Sign up</a>
					<a class="btn btn-outline-light" href="LogoutServlet"><i class="fas fa-sign-out-alt"></i>Log out</a>
					<a class="text-white" style="margin-left:5px">Hello!! ${sessionScope.userInfo.name}さん</a>
				</nav>
		</div>
	</header>
	<div class="container">
			<div class="col-sm-12 text-center">
				<img src="img/${car.image}" width="600" height="450"  style="margin:20px" class="border_radius">
			</div>
		<div class="text-center">
			<ul class="list-group mb-3">
				<li class="list-group-item justify-content-between lh-condensed text-center">
					<h3><c:out value="${car.name}"/></h3>
				</li>
				<li class="list-group-item justify-content-between lh-condensed text-center">
					<h3><c:out value="${car.maker}"/></h3>
				</li>
				<li class="list-group-item justify-content-between lh-condensed text-center">
					<h3><c:out value="${car.year}"/></h3>
				</li>
				<li class="list-group-item justify-content-between lh-condensed d-flex">
					<h6 class="my-0"><c:out value="${car.detail}"/></h6>
				</li>
				<li class="list-group-item justify-content-between text-center">
					<h3>￥<c:out value="${car.price}"/></h3>
				</li>
			</ul>
		</div>
			<div class="row">
				<div class="col-sm-12 text-center">
					<form action="FavoAddServlet" method="post">
						<input type="hidden" name="user_id" value="${sessionScope.userInfo.user_id}">
						<input type="hidden" name="car_id" value="${car.car_id}">
						<input type="submit"  class="button btn btn-primary btn-lg text-right" value="★Favorite!">
						<i class="fas fa-truck-pickup fa-lg icon-blue" style="margin-left:20px"></i>
					<c:choose>
						<c:when test="${car.detail == 'Sold Out!!'}">
							<a class="button btn btn-outline-dark btn-lg text-center" style="width:150px;margin-left:20px"><i class="far fa-times-circle" style="margin-right:5px"></i>SoldOut!!</a>
						</c:when>
						<c:otherwise>
							<a class="button btn btn-primary btn-lg text-center" style="width:150px;margin-left:20px" href="BuyServlet?car_id=${car.car_id}"><i class="far fa-handshake" style="margin-right:5px"></i>Buy it!</a>
						</c:otherwise>
					</c:choose>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 text-left">
					<a class="btn btn-outline-secondary " href="TopServlet">Topページへ</a>
					<a class="btn btn-outline-secondary " style="margin-left:10px" href="CarSearchServlet">在庫一覧へ</a>
				</div>
			</div>
	</div>
<footer class="footer mt-auto py-3">
  <div class="container">
  	<div class="row">
  		<div class="col-6">
   			<span class="text-muted">made by WA TA SHI</span>
   		</div>
   		<div class="col-6 text-right">
   			<span class="text-muted"><a href="LoginAdmin">管理画面</a></span>
   		</div>
    </div>
  </div>
</footer>
</body>
</html>