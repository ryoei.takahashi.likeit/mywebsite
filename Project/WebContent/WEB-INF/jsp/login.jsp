<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/hover-min.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.10.0/css/all.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<header>
		<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-dark border-bottom shadow-sm">
			<a class="my-0 mr-md-auto font-weight-normal btn btn-outline-light" href="TopServlet"><i class="fas fa-home fa-lg" style="margin-right:5px"></i>夢の車屋さん</a>
				<nav class="my-2 my-md-0 mr-md-3">
					<a class="btn btn-outline-light" href="CarSearchServlet?page=1">在庫車一覧</a>
					<a class="btn btn-outline-light"style="margin:10px" href="UserAddServlet">新規登録</a>
					<a class="btn btn-outline-light" href="FavoSearchServlet?user_id=${sessionScope.userInfo.user_id}"><i class="fas fa-star"></i>Favorite</a>
					<a class="btn btn-outline-light" style="margin:10px"href="LoginServlet"><i class="fas fa-file-signature"></i>Sign up</a>
					<a class="btn btn-outline-light" href="LogoutServlet"><i class="fas fa-sign-out-alt"></i>Log out</a>
					<a class="text-white" style="margin-left:5px">Hello!! ${sessionScope.userInfo.name}さん</a>
				</nav>
		</div>
	</header>
		<div class="mx-auto" style="width: 400px;">
			<div class="container">
				<c:if test="${errMsg != null}" >
	 				<div class="alert alert-danger" role="alert">
				 	 ${errMsg}
					</div>
				</c:if>
				<c:if test="${favoMsg != null}" >
	    			<div class="alert alert-danger" role="alert">
		  			${favoMsg}
					</div>
				</c:if>
				<form action="LoginServlet" method="post" class="form-signin  text-center" >
					<img class="mb-4 border_radius_circle" src="img/IMG_1816.jpg" width="200" height="200">
						<c:if test="${cotMsg !=null}">
							<div class="text-center icon-red" style="margin:5px"><h3>${cotMsg}</h3></div>
						</c:if>
					<label for="inputId" class="sr-only">LoginId</label><input type="text" id="inputText" name="login_id" class="form-control"placeholder="Login Id">
						<br>
					<label for="inputPassword" class="sr-only">Password</label><input type="password" id="inputPassword" name="password" class="form-control"placeholder="Password">
						<br>
					<button class="btn btn-lg btn-secondary btn-block hvr-shadow-radial" type="submit">Sign in</button>
				</form>
			<div class="text-center" style="margin-top:50px">
				<a class="btn btn-outline-secondary" href="TopServlet">Back</a>
			</div>
		</div>
</div>
<footer class="footer mt-auto py-3">
  <div class="container">
  	<div class="row">
  		<div class="col-6">
   			<span class="text-muted">made by WA TA SHI</span>
   		</div>
   		<div class="col-6 text-right">
   			<span class="text-muted"><a href="LoginAdmin">管理画面</a></span>
   		</div>
    </div>
  </div>
</footer>
</body>
</html>