<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ようこそ</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/hover-min.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.10.0/css/all.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<header>
		<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-dark border-bottom shadow-sm">
			<a class="my-0 mr-md-auto font-weight-normal btn btn-outline-light" href="TopServlet"><i class="fas fa-home fa-lg" style="margin-right:5px"></i>夢の車屋さん</a>
				<nav class="my-2 my-md-0 mr-md-3">
					<a class="btn btn-outline-light" href="CarSearchServlet?page=1">在庫車一覧</a>
					<a class="btn btn-outline-light"style="margin:10px" href="UserAddServlet">新規登録</a>
					<a class="btn btn-outline-light" href="FavoSearchServlet?user_id=${sessionScope.userInfo.user_id}"><i class="fas fa-star"></i>Favorite</a>
					<a class="btn btn-outline-light" style="margin:10px"href="LoginServlet"><i class="fas fa-file-signature"></i>Sign up</a>
					<a class="btn btn-outline-light" href="LogoutServlet"><i class="fas fa-sign-out-alt"></i>Log out</a>
					<a class="text-white" style="margin-left:5px">Hello!! ${sessionScope.userInfo.name}さん</a>
				</nav>
		</div>
	</header>
<!-- 	ヘッダー下 -->
<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-image">
  <div class="col-md-5 p-lg-5 mx-auto my-5">
    <h1 class="display-4 font-weight-normal text-white">Vintage is Everything!</h1>
    <p class="lead font-weight-normal text-white">電子制御のない自分の力で操る楽しさ</p><p class="text-white">独創性が高いデザイン</p><h2 class="text-white">今にない魅力的な車を貴方に！</h2>
    <a class="btn btn-secondary" href="CarSearchServlet">Let'go!!</a>
  </div>
</div>
<!-- ヘッダー下終了 -->
	<div class="container">
	<c:if test="${alreadyLoginMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${alreadyLoginMsg}
		</div>
	</c:if>
	<c:if test="${notLoginMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${notLoginMsg}
		</div>
	</c:if>
	<c:if test="${LogoutMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${LogoutMsg}
		</div>
	</c:if>
	<c:if test="${alreadyAddMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${alreadyAddMsg}
		</div>
	</c:if>
		<form action="TopServlet" method="post">
			<div class="row">
				<div class="input-field col s8 offset-s2 text-center">
					<i class="fas fa-search" style="margin:10px"></i><input type="text" name="word" placeholder=" フリーワード（数字は半角入力）" style="width:400px"><br>
					<input type="submit" class="btn btn-primary" value="Search">
				</div>
			</div>
		</form>
	<div class="row" style="margin:30px">
		<div class="input-field col s8 offset-s2 text-center">
			<h2><i class="fas fa-fire-alt icon-red" style="margin:10px"></i>Hot Cars<i class="fas fa-fire-alt icon-red"style="margin:10px"></i></h2>
		</div>
	</div>
	<div class="section">
		<!--   おすすめ商品   -->
		<div class="row">
			<c:forEach var="car" items="${carList}">
			<div class="col-4">
				<div class="card" style="width: 18rem;height:480px">
					<img src="img/${car.image}" class="card-img-top">
					<div class="card-body">
						<h5 class="card-title">${car.name}</h5>
						<p class="card-text cardDetail_overflow">${car.detail}</p>
						<a href="CarDetailServlet?car_id=${car.car_id}" class="btn btn-primary">詳細ページへ</a>
					</div>
				</div>
			</div>
		</c:forEach>
		</div>
	</div>
</div>
<footer class="footer mt-auto py-3">
  <div class="container">
  	<div class="row">
  		<div class="col-6">
   			<span class="text-muted">made by WA TA SHI</span>
   		</div>
   		<c:if test="${sessionScope.userInfo.login_id == 'admin'}">
   		<div class="col-6 text-right">
   			<span class="text-muted"><a href="LoginAdmin">管理画面</a></span>
   		</div>
   		</c:if>
    </div>
  </div>
</footer>
</body>
</html>