<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>車両管理</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/hover-min.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.10.0/css/all.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<header>
		<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-dark border-bottom shadow-sm">
			<a class="my-0 mr-md-auto font-weight-normal btn btn-outline-light" href="TopServlet"><i class="fas fa-home fa-lg" style="margin-right:5px"></i>夢の車屋さん</a>
				<nav class="my-2 my-md-0 mr-md-3">
					<a class="btn btn-outline-light" href="CarSearchServlet?page=1">在庫車一覧</a>
					<a class="btn btn-outline-light"style="margin:10px" href="UserAddServlet">新規登録</a>
					<a class="btn btn-outline-light" href="FavoSearchServlet?user_id=${sessionScope.userInfo.user_id}"><i class="fas fa-star"></i>Favorite</a>
					<a class="btn btn-outline-light" style="margin:10px"href="LoginServlet"><i class="fas fa-file-signature"></i>Sign up</a>
					<a class="btn btn-outline-light" href="LogoutServlet"><i class="fas fa-sign-out-alt"></i>Log out</a>
					<a class="text-white" style="margin-left:5px">Hello!! ${sessionScope.userInfo.name}さん</a>
				</nav>
		</div>
	</header>
	<div class="container">
		<div class="text-center" style="margin:20px">
			<h2>在庫車　管理画面</h2>
				<i class="fas fa-car-side fa-lg icon-blue"></i><a class="btn btn-primary btn-lg" href="CarAddServlet" style="margin:20px">在庫車　新規登録</a>
		</div>
			<form action="CarSearchServlet" method="post">
			<div class="row">
				<div class="col-4 text-right">メーカー名</div>

				<div class="col-8">
					<div class="form-group">
						<input type="text" class="form-control" id="exampleInputId1"
							placeholder="メーカー名" name="maker" style="width:445px">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-4 text-right">車名</div>
				<div class="col-8">
					<div class="form-group">
						<input type="text" class="form-control" style="width:445px"
							id="exampleInputPassword1" placeholder="車名" name="name">
					</div>
					<div class="form-group form-check"></div>
				</div>
			</div>

			<div class="row">

				<div class="col-4 text-right">年式</div>

				<div class="col-2">
					<div class="form-group">
						<input type="number" class="form-control" id="exampleInputEmail1"
							placeholder="年式" name="stYear">
					</div>
				</div>
				<div class="col-1 text-center">
					<h2>～</h2>
				</div>
				<div class="col-2">
					<div class="form-group">
						<input type="number" class="form-control" id="exampleInputEmail1"
							placeholder="年式" name="edYear">
					</div>
				</div>
			</div>
			<div align="center">
				<button type="submit" class="btn btn-primary">検索</button>
			</div>
		</form>
			<div class="text-left" style="margin:30px">
				<a class="btn btn-outline-secondary" href="adminServlet">Back</a>
			</div>
		<table class="table" style="margin:20px">
			<thead class="table-bordered">
				<tr>
					<th scope="col">メーカー名</th>
					<th scope="col">車名</th>
					<th scope="col">年式</th>
					<th scope="col">最終更新日</th>
					<th scope="col"></th>
				</tr>
			</thead>
			<tbody>
			 <c:forEach var="list" items="${carList}">
				<tr>
					<th scope="row">${list.maker}</th>
					<td>${list.name}</td>
					<td>${list.year}</td>
					<td>${list.update_date}</td>
                     <td>
                       <a class="btn btn-primary" href="CarAdminDetailServlet?car_id=${list.car_id}">詳細</a>
                       	<a class="btn btn-success" href="CarUpdateServlet?car_id=${list.car_id}">更新</a>
                       <a class="btn btn-danger" href ="CarDeleteServlet?car_id=${list.car_id}">削除</a>
                     </td>
				</tr>
			</c:forEach>
				<tr>
			</tbody>
		</table>
	</div>
<footer class="footer mt-auto py-3">
  <div class="container">
  	<div class="row">
  		<div class="col-6">
   			<span class="text-muted">made by WA TA SHI</span>
   		</div>
   		<div class="col-6 text-right">
   			<span class="text-muted"><a href="LoginAdmin">管理画面</a></span>
   		</div>
    </div>
  </div>
</footer>
</body>
</html>