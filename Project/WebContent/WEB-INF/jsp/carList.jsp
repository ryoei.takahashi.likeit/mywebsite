<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
<title>在庫車一覧</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/hover-min.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.10.0/css/all.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="Materialize/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection" />
<link href="Materialize/css/style.css" type="text/css" rel="stylesheet" media="screen,projection" />
</head>
<body>
	<header>
		<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-dark border-bottom shadow-sm">
			<a class="my-0 mr-md-auto font-weight-normal btn btn-outline-light" href="TopServlet"><i class="fas fa-home fa-lg" style="margin-right:5px"></i>夢の車屋さん</a>
				<nav class="my-2 my-md-0 mr-md-3">
					<a class="btn btn-outline-light" href="CarSearchServlet?page=1">在庫車一覧</a>
					<a class="btn btn-outline-light"style="margin:10px" href="UserAddServlet">新規登録</a>
					<a class="btn btn-outline-light" href="FavoSearchServlet?user_id=${sessionScope.userInfo.user_id}"><i class="fas fa-star"></i>Favorite</a>
					<a class="btn btn-outline-light" style="margin:10px"href="LoginServlet"><i class="fas fa-file-signature"></i>Sign up</a>
					<a class="btn btn-outline-light" href="LogoutServlet"><i class="fas fa-sign-out-alt"></i>Log out</a>
					<a class="text-white" style="margin-left:5px">Hello!! ${sessionScope.userInfo.name}さん</a>
				</nav>
		</div>
	</header>
	<div class="container">
		<form action="TopServlet" method="post">
			<div class="row">
				<div class="input-field col s8 offset-s2 text-center">
					<i class="fas fa-search" style="margin-top:50px"></i><input type="text" name="word" placeholder=" フリーワード（数字は半角入力）" style="width:400px;margin-left:10px"><br>
					<input type="submit" class="btn btn-primary" value="Search" style="margin:10px">
				</div>
			</div>
		</form>
	</div>
	<div class="container">
		<h5 class="text-center">ヒット件数：${cnt}件</h5><br>
		<div class="row">
			<c:forEach var="list" items="${carList}">
				<div class="col-4">
					<div class="card" style="width: 18rem;margin:20px;height:510px">
						<img src="img/${list.image}" class="card-img-top">
						<div class="card-body">
							<h5 class="card-title">${list.maker}  ${list.name}</h5><h5>￥${list.price}</h5>
							<p class="cardDetail_overflow">${list.detail}</p>
							<a href="CarDetailServlet?car_id=${list.car_id}" class="btn btn-primary">詳細ページへ</a>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
		<nav aria-label="Page navigation example">
 			 <ul class="pagination justify-content-center">
 			 <!-- back  -->
				<c:choose>
					<c:when test="${page == 1}">
						<li class="page-item disabled"><a class="page-link">Back</a></li>
					</c:when>
					<c:otherwise>
						<li class="page-item"><a class="page-link" href="CarSearchServlet?word=${word}&page=${page - 1}">Back</a></li>
					</c:otherwise>
				</c:choose>

				<!-- ページインデックス -->
				<c:forEach begin="${(page - 3) > 0 ? page - 3 : 1}" end="${(page + 3) > pageMax ? pageMax : page + 3}" step="1" varStatus="status">
					<li class="page-item"<c:if test="${page == status.index }"></c:if>><a  class="page-link" href="CarSearchServlet?word=${word}&page=${status.index}">${status.index}</a></li>
				</c:forEach>

				<!-- 1ページ送るボタン -->
				<c:choose>
				<c:when test="${page == pageMax || pageMax == 0}">
					<li class="page-item"><a class="page-link">Next</a></li>
				</c:when>
				<c:otherwise>
					<li class="page-item"><a class="page-link" href="CarSearchServlet?word=${word}&page=${page + 1}">Next</a></li>
				</c:otherwise>
				</c:choose>
			</ul>
		</nav>
 	</div>
<footer class="footer mt-auto py-3">
  <div class="container">
  	<div class="row">
  		<div class="col-6">
   			<span class="text-muted">made by WA TA SHI</span>
   		</div>
   		<div class="col-6 text-right">
   			<span class="text-muted"><a href="LoginAdmin">管理画面</a></span>
   		</div>
    </div>
  </div>
</footer>
</body>
</html>