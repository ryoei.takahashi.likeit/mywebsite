package beans;

import java.io.Serializable;
import java.sql.Date;


public class BuyBeans  implements Serializable {

	private int buy_id;
	private int user_id;
	private int car_id;
	private int option_id;
	private int total_price;
	private int price;
	private Date create_date;

	private String user_name;
	private String car_name;
	private String menu;
	private int op_price;
	private String car_maker;

	public BuyBeans(String user_name, String car_name, String menu,int price, int total_price, int buy_id,Date createDate) {
		this.user_name = user_name;
		this.car_name = car_name;
		this.menu = menu;
		this.price = price;
		this.total_price = total_price;
		this.buy_id = buy_id;
		this.create_date = createDate;
	}

	public BuyBeans(String user_name, String car_name, String car_maker, String menu, int price,int op_price, int total_price, Date createDate) {
		this.user_name = user_name;
		this.car_name = car_name;
		this.car_maker = car_maker;
		this.menu = menu;
		this.price = price;
		this.op_price =op_price;
		this.total_price = total_price;
		this.create_date = createDate;
	}

	public int getBuy_id() {
		return buy_id;
	}
	public void setBuy_id(int buy_id) {
		this.buy_id = buy_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getCar_id() {
		return car_id;
	}
	public void setCar_id(int car_id) {
		this.car_id = car_id;
	}
	public int getOption_id() {
		return option_id;
	}
	public void setOption_id(int option_id) {
		this.option_id = option_id;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getTotal_price() {
		return total_price;
	}
	public void setTotal_price(int total_price) {
		this.total_price = total_price;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getCar_name() {
		return car_name;
	}

	public void setCar_name(String car_name) {
		this.car_name = car_name;
	}

	public String getMenu() {
		return menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}
	public int getOp_price() {
		return op_price;
	}

	public void setOp_price(int op_price) {
		this.op_price = op_price;
	}
	public String getCar_maker() {
		return car_maker;
	}

	public void setCar_maker(String car_maker) {
		this.car_maker = car_maker;
	}


}