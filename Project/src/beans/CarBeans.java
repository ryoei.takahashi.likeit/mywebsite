package beans;

import java.io.Serializable;
import java.sql.Date;


public class CarBeans  implements Serializable {

	private int car_id;
	private String name;
	private String detail;
	private String year;
	private String maker;
	private int price;
	private Date create_date;
	private Date update_date;
	private String image;

//	お客さん用検索コンストラクタ
	public CarBeans(int car_id, int price, String name, String detail, String year, String maker, String image) {
		this.car_id = car_id;
		this.name = name;
		this.detail = detail;
		this.year = year;
		this.maker = maker;
		this.price = price;
		this.image = image;
	}

//	admin用検索コンストラクタ
	public CarBeans(int car_id,  String name, String detail, String year, String maker,int price, String image,
			Date update_Date, Date create_Date) {
		this.car_id = car_id;
		this.name = name;
		this.detail = detail;
		this.year = year;
		this.maker = maker;
		this.price = price;
		this.image = image;
		this.update_date = update_Date;
		this.create_date = create_Date;
	}


	public int getCar_id() {
		return car_id;
	}
	public void setCar_id(int car_id) {
		this.car_id = car_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMaker() {
		return maker;
	}
	public void setMaker(String maker) {
		this.maker = maker;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public Date getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
}