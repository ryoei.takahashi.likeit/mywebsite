package beans;

import java.io.Serializable;
import java.sql.Date;


public class UserBeans  implements Serializable {

	private int user_id;
	private String login_id;
	private String name;
	private Date birth_date;
	private Date create_date;
	private String password;

//	ログイン用
	public UserBeans(String login_id, String name,int user_id) {
		this.login_id= login_id;
		this.name = name;
		this.user_id=user_id;
	}

//	検索用
	public UserBeans(int user_id, String login_id, String name, Date birth_date, String password, Date create_date) {
		this.user_id = user_id;
		this.login_id= login_id;
		this.name = name;
		this.birth_date = birth_date;
		this.password = password;
		this.create_date = create_date;
	}

	public UserBeans() {
	}

	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirth_date() {
		return birth_date;
	}

	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}