package site;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CarBeans;
import dao.CarDao;

/**
 * Servlet implementation class TopServlet
 */
@WebServlet("/TopServlet")
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final int PAGE_MAX_ITEM_COUNT = 6;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public TopServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		CarDao carDao = new CarDao();
		List<CarBeans>carList = carDao.getRandItem(3);

		request.setAttribute("carList", carList);
		request.getRequestDispatcher("/WEB-INF/jsp/Top.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		int page = Integer.parseInt(request.getParameter("page") == null ? "1" : request.getParameter("page"));

		try {
		String word = request.getParameter("word");
		session.setAttribute("word", word);
//		検索結果
		CarDao carDao = new CarDao();
		List<CarBeans> carList = carDao.searchFree(word,page,PAGE_MAX_ITEM_COUNT);

//		検索件数
		double CarCount = carDao.getCarCount(word);
//		総ページ数（件数/1ページの表示数）
		int pageMax = (int) Math.ceil(CarCount / PAGE_MAX_ITEM_COUNT);

		request.setAttribute("carList", carList);
		request.setAttribute("cnt",(int) CarCount);
		request.setAttribute("pageMax", pageMax);
		request.setAttribute("page",page);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/carList.jsp");
		dispatcher.forward(request, response);
	}

}