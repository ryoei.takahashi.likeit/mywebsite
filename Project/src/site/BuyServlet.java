package site;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CarBeans;
import beans.OptionBeans;
import beans.UserBeans;
import dao.CarDao;
import dao.OptionDao;

/**
 * Servlet implementation class BuyServlet
 */
@WebServlet("/BuyServlet")
public class BuyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BuyServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");
			if (userInfo == null) {
				response.sendRedirect("LoginServlet");
				return;
			}

			String id = request.getParameter("car_id");
			System.out.println(id);
			int carId = Integer.parseInt(id);

			CarDao carDao = new CarDao();
			OptionDao opDao = new OptionDao();

			CarBeans car = carDao.findById(carId);
			List<OptionBeans> optionList = opDao.getAll();

			request.setAttribute("car", car);
			request.setAttribute("optionList", optionList);
			for(OptionBeans op: optionList) {
				System.out.println(op.getMenu());
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buy.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {

		try {
		String opidSt= request.getParameter("optionId");
		String idSt = request.getParameter("car_id");
		System.out.println(idSt);
		int carId = Integer.parseInt(idSt);
		int opid = Integer.parseInt(opidSt);

		CarDao carDao = new CarDao();
		CarBeans car = carDao.findById(carId);

		OptionDao opDao = new OptionDao();
		OptionBeans op = opDao.getOptionById(opid);

		request.setAttribute("car", car);
		request.setAttribute("option",op);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyConfirm.jsp");
		dispatcher.forward(request, response);
	}

}
