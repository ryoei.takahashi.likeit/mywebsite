package site;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CarBeans;
import beans.UserBeans;
import dao.CarDao;
import dao.FavoDao;

/**
 * Servlet implementation class FavoAddServlet
 */
@WebServlet("/FavoAddServlet")
public class FavoAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public FavoAddServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans userInfo =(UserBeans)session.getAttribute("userInfo");
		if(userInfo == null) {
			request.setAttribute("favoMsg", "お気に入り登録にはログインが必要です。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		String carSt = request.getParameter("car_id");
		String userSt =request.getParameter("user_id");

		int car_id = Integer.parseInt(carSt);
		int user_id = Integer.parseInt(userSt);

		//favo動作
		FavoDao favoDao = new FavoDao();
		try {
			favoDao.addFavo(user_id,car_id);
		} catch (SQLException e) {
			e.printStackTrace();
		}

//		carid動作
		CarDao carDao = new CarDao();
		CarBeans car = carDao.findById(car_id);

		request.setAttribute("car", car);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/FavoAddResult.jsp");
		dispatcher.forward(request, response);
	}

}
