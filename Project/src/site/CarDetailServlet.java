package site;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CarBeans;
import dao.CarDao;

/**
 * Servlet implementation class CarDetailServlet
 */
@WebServlet("/CarDetailServlet")
public class CarDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CarDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("car_id");
		System.out.println(id);
		int carId = Integer.parseInt(id);


		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		CarDao carDao = new CarDao();
		CarBeans car = carDao.findById(carId);


		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード

		request.setAttribute("car", car);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/carDetail.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
