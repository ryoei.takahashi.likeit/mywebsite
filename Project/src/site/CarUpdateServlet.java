package site;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CarBeans;
import dao.CarDao;

/**
 * Servlet implementation class CarUpdateServlet
 */
@WebServlet("/CarUpdateServlet")
public class CarUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CarUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String idSt = request.getParameter("car_id");
		System.out.println(idSt);

		int id = Integer.parseInt(idSt);

		CarDao carDao = new CarDao();
		CarBeans car = carDao.findById(id);

		request.setAttribute("car", car);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/adminUpdate.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String maker = request.getParameter("maker");
		String name = request.getParameter("name");
		String priceSt = request.getParameter("price");
		String year = request.getParameter("year");
		String detail = request.getParameter("detail");
		String image = request.getParameter("image");
		String idSt = request.getParameter("car_id");

		int id = Integer.parseInt(idSt);

		int price = Integer.parseInt(priceSt);

		CarDao CarDao = new CarDao();
		try {
			CarDao.UpdateCar(maker,name,price,year,detail,image,id);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		response.sendRedirect("CarAdminServlet");
	}

}
