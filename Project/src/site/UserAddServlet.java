package site;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CarBeans;
import beans.UserBeans;
import dao.CarDao;
import dao.UserDao;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");
		if (userInfo != null) {
			request.setAttribute("alreadyAddMsg", "既に登録済みです。");

			CarDao carDao = new CarDao();
			List<CarBeans>carList = carDao.getRandItem(3);

			request.setAttribute("carList", carList);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Top.jsp");
			dispatcher.forward(request, response);
			return;
		}

		request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birth_date");
		String password2 = request.getParameter("password2");

		boolean isError = false;
		// 空白チェック
		if (login_id.equals("") || password.equals("") || password2.equals("") || name.equals("")
				|| birth_date.equals("")) {
			isError = true;
		}

		// パスワード一致チェック
		if (!password.equals(password2)) {
			isError = true;
		}

		UserDao UserDao = new UserDao();
		boolean isLoginIdCheck = UserDao.findLoginId(login_id);

		// 既存ログインID一致チェック
		if (isLoginIdCheck) {
			isError = true;
		}

		if (isError) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			UserBeans user2 = new UserBeans();
			user2.setLogin_id(login_id);
			user2.setName(name);

			request.setAttribute("user", user2);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
			dispatcher.forward(request, response);
		} else {
			UserDao userDao = new UserDao();
			userDao.insertUser(login_id, password, name, birth_date);
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAddResult.jsp");
		dispatcher.forward(request, response);
	}
}
