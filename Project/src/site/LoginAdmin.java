package site;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class LoginAdmin
 */
@WebServlet("/LoginAdmin")
public class LoginAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginAdmin() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		request.setAttribute("cotMsg", "管理者用画面です");

		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

//		ログインなし
		if (userInfo == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/loginAdmin.jsp");
			dispatcher.forward(request, response);
		}

//		管理者判定
		String adminid = userInfo.getLogin_id();
		if (userInfo != null && adminid.equals("admin")) {
			response.sendRedirect("adminServlet");
		} else {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/loginAdmin.jsp");
		dispatcher.forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
			request.setCharacterEncoding("UTF-8");

			String loginId = request.getParameter("login_id");
			String password = request.getParameter("password");

			if(!loginId.equals("admin")) {
				request.setAttribute("notAdminMsg", "管理者以外はアクセスできません。");

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/loginAdmin.jsp");
				dispatcher.forward(request, response);
				return;
			}

			UserDao userDao = new UserDao();
			UserBeans user = userDao.findByLoginInfo(loginId, password);

			if (user == null) {
				request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります。");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/loginAdmin.jsp");
				dispatcher.forward(request, response);
				return;
			}
			HttpSession session = request.getSession();
			session.setAttribute("userInfo", user);
			response.sendRedirect("adminServlet");
	}

}
