package site;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.FavoBeans;
import beans.UserBeans;
import dao.FavoDao;

/**
 * Servlet implementation class FavoSearchServlet
 */
@WebServlet("/FavoSearchServlet")
public class FavoSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public FavoSearchServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");
		if (userInfo == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		try {
		String id = request.getParameter("user_id");

		FavoDao favoDao = new FavoDao();
		List<FavoBeans> favoList = favoDao.getAll(id);

//
//		if (favoList == null) {
//			request.setAttribute("errMsg", "お気に入りが登録されていません。");
//
//			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/favorite.jsp");
//			dispatcher.forward(request, response);
//			return;
//		}

		request.setAttribute("favoList", favoList);

		request.getRequestDispatcher("/WEB-INF/jsp/favorite.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
