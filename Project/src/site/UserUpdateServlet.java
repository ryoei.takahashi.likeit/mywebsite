package site;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("user_id");
		System.out.println(id);

//		int id = Integer.parseInt(idSt);

		UserDao userDao = new UserDao();
		UserBeans user = userDao.findById(id);

		request.setAttribute("user", user);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birth_date");
		String id = request.getParameter("user_id");
		String loginId = request.getParameter("login_id");
		String password2 = request.getParameter("password2");

		boolean isError = false;

		if (password.equals("") || password2.equals("") || name.equals("")
				|| birth_date.equals("")) {
			isError = true;
		}

		if (!password.equals(password2)) {
			isError = true;
		}

		if(isError) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			UserBeans user2 = new UserBeans();
			user2.setLogin_id(loginId);
			user2.setName(name);

			request.setAttribute("user", user2);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
		}

		UserDao UserDao = new UserDao();
		UserDao.UpdateUser(password, name, birth_date,id);

		response.sendRedirect("UserAdminServlet");
	}

}
