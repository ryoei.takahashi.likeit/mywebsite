package site;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.FavoBeans;
import beans.UserBeans;
import dao.FavoDao;

/**
 * Servlet implementation class FavoDeleteServlet
 */
@WebServlet("/FavoDeleteServlet")
public class FavoDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public FavoDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		UserBeans userInfo =(UserBeans)session.getAttribute("userInfo");
		if(userInfo == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		String id = request.getParameter("favo_id");

		FavoDao favoDao = new FavoDao();
		FavoBeans favo;

		favo = favoDao.findById(id);
		request.setAttribute("favo", favo);

		 request.getRequestDispatcher("/WEB-INF/jsp/favoDelete.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserBeans userInfo =(UserBeans)session.getAttribute("userInfo");
		if(userInfo == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		String idSt = request.getParameter("favo_id");
		String userid = request.getParameter("user_id");

		int id = Integer.parseInt(idSt);

		FavoDao favoDao = new FavoDao();
		favoDao.deleteFavo(id);

		List<FavoBeans> favoList;

		try {
			favoList = favoDao.getAll(userid);
			 request.setAttribute("favoList", favoList);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		request.getRequestDispatcher("/WEB-INF/jsp/favorite.jsp").forward(request, response);
	}

}
