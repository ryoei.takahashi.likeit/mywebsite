package site;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.CarDao;

/**
 * Servlet implementation class CarAddServlet
 */
@WebServlet("/CarAddServlet")
public class CarAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CarAddServlet() {
        super();

    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		 request.getRequestDispatcher("/WEB-INF/jsp/adminAdd.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String maker = request.getParameter("maker");
		String name = request.getParameter("name");
		String priceSt = request.getParameter("price");
		String year = request.getParameter("year");
		String detail = request.getParameter("detail");
		String image = request.getParameter("image");

		int price = Integer.parseInt(priceSt);

		CarDao CarDao = new CarDao();
		try {
			CarDao.insertCar(maker,name,price,year,detail,image);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		response.sendRedirect("CarAdminServlet");
	}

}
