package site;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("user_id");
		System.out.println(id);

//		int id = Integer.parseInt(idSt);

		UserDao userDao = new UserDao();
		UserBeans user= userDao.findById(id);

		request.setAttribute("user", user);

		request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("user_id");
//		int id = Integer.parseInt(idSt);

		UserDao userDao = new UserDao();
		userDao.deleteUser(id);

		response.sendRedirect("UserAdminServlet");
	}
}
