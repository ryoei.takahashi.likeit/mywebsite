package site;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CarBeans;
import dao.CarDao;

/**
 * Servlet implementation class CarDeleteServlet
 */
@WebServlet("/CarDeleteServlet")
public class CarDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CarDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idSt = request.getParameter("car_id");
		System.out.println(idSt);

		int id = Integer.parseInt(idSt);

		CarDao carDao = new CarDao();
		CarBeans car= carDao.findById(id);

		request.setAttribute("car", car);

		request.getRequestDispatcher("/WEB-INF/jsp/adminDelete.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idSt = request.getParameter("car_id");
		int id = Integer.parseInt(idSt);

		CarDao carDao = new CarDao();
		carDao.deleteCar(id);

		response.sendRedirect("CarAdminServlet");
	}

}
