package site;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CarBeans;
import beans.OptionBeans;
import dao.BuyDao;
import dao.CarDao;
import dao.OptionDao;

/**
 * Servlet implementation class BuyResultServlet
 */
@WebServlet("/BuyResultServlet")
public class BuyResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyResultServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		try {
		String user_idSt = request.getParameter("user_id");
		String car_idSt = request.getParameter("car_id");
		String op_idSt = request.getParameter("optionId");
		String priceSt = request.getParameter("total_price");

		int user_id = Integer.parseInt(user_idSt);
		int car_id = Integer.parseInt(car_idSt);
		int option_id = Integer.parseInt(op_idSt);
		int total_price = Integer.parseInt(priceSt);

//		購入処理
		BuyDao buyDao = new BuyDao();
		buyDao.CreateBuyHistory(user_id, car_id, option_id, total_price);
		buyDao.SoldCar(car_id);

//		フォワード処理
		CarDao carDao = new CarDao();
		OptionDao opDao = new OptionDao();


		CarBeans car = carDao.findById(car_id);
		OptionBeans op = opDao.getOptionById(option_id);

		request.setAttribute("car", car);
		request.setAttribute("op", op);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyResult.jsp");
		dispatcher.forward(request, response);

	}
}
