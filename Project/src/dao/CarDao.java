package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.CarBeans;


public class CarDao {

	//	車全検索
	public List<CarBeans> getCarAll(int page,int maxCar) {

		Connection conn = null;
		PreparedStatement stmt =null;
		try {
			int startiItemNum = (page - 1) * maxCar;
			conn = DBManager.getConnection();

			stmt =conn.prepareStatement("SELECT * from car where maker != 'Escape' LIMIT ?,? ");
			stmt.setInt(1, startiItemNum);
			stmt.setInt(2, maxCar);

			List<CarBeans> carList = new ArrayList<CarBeans>();
			ResultSet rs = stmt.executeQuery();

				while (rs.next()) {
					int car_id = rs.getInt("car_id");
					int price =rs.getInt("price");
					String name = rs.getString("name");
					String detail = rs.getString("detail");
					String year = rs.getString("year");
					String maker = rs.getString("maker");
					String image =rs.getString("image");
					Date updateDate = rs.getDate("update_date");
					Date createDate = rs.getDate("create_date");

					CarBeans car = new CarBeans(car_id, name,detail,year,maker,price,image, createDate, updateDate);
					carList.add(car);
				}

				return carList;

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}

	//	車全検索 admin用
	public List<CarBeans> getCarAllAdmin() {

		Connection conn = null;
		PreparedStatement stmt =null;
		try {

			conn = DBManager.getConnection();

			stmt =conn.prepareStatement("SELECT * from car where maker != 'Escape'");

			List<CarBeans> carList = new ArrayList<CarBeans>();
			ResultSet rs = stmt.executeQuery();

				while (rs.next()) {
					int car_id = rs.getInt("car_id");
					int price =rs.getInt("price");
					String name = rs.getString("name");
					String detail = rs.getString("detail");
					String year = rs.getString("year");
					String maker = rs.getString("maker");
					String image =rs.getString("image");
					Date updateDate = rs.getDate("update_date");
					Date createDate = rs.getDate("create_date");

					CarBeans car = new CarBeans(car_id, name,detail,year,maker,price,image, createDate, updateDate);
					carList.add(car);
				}

				return carList;

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}

//	car_idでの検索
	public CarBeans findById(int carId) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM car WHERE car_id = ? and maker != 'Escape'";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, carId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int car_id = rs.getInt("car_id");
			String name = rs.getString("name");
			String detail = rs.getString("detail");
			String year = rs.getString("year");
			String maker = rs.getString("maker");
			int price = rs.getInt("price");
			String image =rs.getString("image");
			Date updateDate = rs.getDate("update_date");
			Date createDate = rs.getDate("create_date");

			CarBeans car = new CarBeans(car_id, name,detail,year,maker,price,image, createDate, updateDate);
			return car;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}



//	車名　メーカー　年式検索 admin用
	public List<CarBeans> search(String name, String maker, String stYear,String edYear) {
		Connection conn = null;
		List<CarBeans> carList = new ArrayList<CarBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT * from car where maker != 'Escape'";

			if(!name.equals(""))  {
				sql += " and  name like '%" + name + "%'";
			}

			if(!maker.equals(""))  {
				sql += " and  maker like '%" + maker + "%'";

			}
			if(!stYear.equals("")) {
				sql += " and year >= '" + stYear +"'";
			}

			if(!edYear.equals("")) {
				sql += " and year <= '" + edYear +"'";
			}
			// SELECTを実行し、結果表を取得

			System.out.println(sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int car_id = rs.getInt("car_id");
				String Rname = rs.getString("name");
				String detail = rs.getString("detail");
				String year = rs.getString("year");
				String Rmaker = rs.getString("maker");
				int price = rs.getInt("price");
				String image =rs.getString("image");
				Date updateDate = rs.getDate("update_date");
				Date createDate = rs.getDate("create_date");

				CarBeans car = new CarBeans(car_id, Rname,detail,year,Rmaker,price,image, createDate, updateDate);

				carList.add(car);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return carList;
	}

//	車の新規登録
	public void insertCar(String maker, String name,int price,String year,String detail,String image) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO car(maker,name,price,year,detail,image,create_date,update_date) VALUES(?,?,?,?,?,?,now(),now())");
			st.setString(1,maker);
			st.setString(2,name);
			st.setInt(3,price);
			st.setString(4,year);
			st.setString(5,detail);
			st.setString(6,image);
			st.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

//	車情報の更新
	public void UpdateCar(String maker, String name, int price,String year,String detail,String image, int car_id) throws SQLException {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "update car set maker=?,name =?,price =?,year =?,detail =?,image =?,update_date = now() where car_id =?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, maker);
			pStmt.setString(2, name);
			pStmt.setInt(3, price);
			pStmt.setString(4, year);
			pStmt.setString(5, detail);
			pStmt.setString(6,image);
			pStmt.setInt(7, car_id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}

	}

//	車の登録削除
	public void deleteCar(int id) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "delete from car where car_id =?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
//	おすすめ生成
	public List<CarBeans> getRandItem(int limit){

		Connection conn = null;
		List<CarBeans> carList = new ArrayList<CarBeans>();
		try {

				conn = DBManager.getConnection();

				String sql = "SELECT * from car where maker != 'Escape' ORDER BY RAND() LIMIT ?";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setInt(1, limit);

				ResultSet rs = pStmt.executeQuery();

				while (rs.next()) {
					int car_id = rs.getInt("car_id");
					int price =rs.getInt("price");
					String name = rs.getString("name");
					String detail = rs.getString("detail");
					String year = rs.getString("year");
					String maker = rs.getString("maker");
					String image =rs.getString("image");
					CarBeans car = new CarBeans(car_id, price,name, detail,year,maker,image);

					carList.add(car);
			}
			return carList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

//	Topと一覧内の検索
	public List<CarBeans> searchFree(String word,int page,int maxCar) {
		Connection conn = null;
		List<CarBeans> carList = new ArrayList<CarBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			int startiItemNum = (page - 1) * maxCar;

			String sql = " SELECT * from car where "
					+ "(maker like '%" + word + "%' or name like '%" + word + "%' or year like '%"+word+"%' or detail like '%"+word+"%') and maker != 'Escape'"
					+ " order by car_id limit "+ startiItemNum +" ,"+ maxCar +";";

			System.out.println(sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int car_id = rs.getInt("car_id");
				String Rname = rs.getString("name");
				String detail = rs.getString("detail");
				String year = rs.getString("year");
				String Rmaker = rs.getString("maker");
				int price = rs.getInt("price");
				String image =rs.getString("image");
				Date updateDate = rs.getDate("update_date");
				Date createDate = rs.getDate("create_date");

				CarBeans car = new CarBeans(car_id, Rname,detail,year,Rmaker,price,image, createDate, updateDate);

				carList.add(car);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return carList;
	}

//	ヒット件数のカウント

	public double getCarCount(String word) throws SQLException {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = " select count(*) as cnt from car where "
					+ "(maker like '%" + word + "%' or name like '%" + word + "%' or year like '%"+word+"%' or detail like '%"+word+"%') and maker != 'Escape'";

			System.out.println(sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);


			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}
}
