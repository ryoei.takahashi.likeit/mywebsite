package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.FavoBeans;


public class FavoDao {

	public List<FavoBeans> getAll(String id) throws SQLException {
		Connection con = null;
		List<FavoBeans> favoList = new ArrayList<FavoBeans>();

		try {
			con = DBManager.getConnection();

			String sql = "SELECT * FROM favo inner join car on favo.car_id = car.car_id where user_id=?";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int car_id = rs.getInt("car_id");
				int price =rs.getInt("price");
				String name = rs.getString("name");
				String detail = rs.getString("detail");
				String year = rs.getString("year");
				String maker = rs.getString("maker");
				String image =rs.getString("image");
				int favo_id =rs.getInt("favo_id");
				int user_id=rs.getInt("user_id");
				FavoBeans favo = new FavoBeans(car_id, name,detail,year,maker,price,image,favo_id,user_id);

				favoList.add(favo);
			}
			return favoList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

//	favoidでの検索
	public FavoBeans findById(String favoId) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM favo inner join car on favo.car_id = car.car_id where favo_id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, favoId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int car_id = rs.getInt("car_id");
			String name = rs.getString("name");
			String detail = rs.getString("detail");
			String year = rs.getString("year");
			String maker = rs.getString("maker");
			int price = rs.getInt("price");
			String image =rs.getString("image");
			int favo_id =rs.getInt("favo_id");
			int user_id=rs.getInt("user_id");

			FavoBeans favo = new FavoBeans(car_id, name,detail,year,maker,price,image,favo_id,user_id);
			return favo;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

//	お気に入り追加
	public void addFavo(int user_id,int car_id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO favo(user_id,car_id) VALUES(?,?)");
			st.setInt(1,user_id);
			st.setInt(2,car_id);
			st.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

//	お気に入り削除
	public void deleteFavo(int id) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "delete from favo where favo_id =?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}