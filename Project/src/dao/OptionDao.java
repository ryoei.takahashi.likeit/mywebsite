package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.OptionBeans;

public class OptionDao {

	public List<OptionBeans> getAll() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM optionmenu where option_id != 4");

			ResultSet rs = st.executeQuery();

			List<OptionBeans> option = new ArrayList<OptionBeans>();
			while (rs.next()) {
				OptionBeans op = new OptionBeans();
				op.setOptionId(rs.getInt("option_id"));
				op.setMenu(rs.getString("menu"));
				op.setPrice(rs.getInt("price"));
				option.add(op);
			}
			return option;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public OptionBeans getOptionById(int optionId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM optionmenu WHERE option_id = ?");
			st.setInt(1, optionId);

			ResultSet rs = st.executeQuery();

			OptionBeans op = new OptionBeans();
			if (rs.next()) {
				op.setOptionId(rs.getInt("option_id"));
				op.setMenu(rs.getString("menu"));
				op.setPrice(rs.getInt("price"));
			}
			return op;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

//
//     購入IDによる配送方法検索
	public static OptionBeans getOptionBeansByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT option.name,"
					+ " optionmenu.price"
					+ " FROM buy"
					+ " JOIN optionmenu"
					+ " ON optionmenu.option_id = buy.option_id"
					+ " WHERE buy.buy_id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			OptionBeans op = new OptionBeans();

			while (rs.next()) {
				op.setMenu(rs.getString("name"));
				op.setPrice(rs.getInt("optionmenu.price"));

			}

			System.out.println("searching option by BuyID has been completed");
			return op;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}

