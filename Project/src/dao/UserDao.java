package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserBeans;

/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao {

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 */
	public UserBeans findByLoginInfo(String login_id, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, login_id);
			pStmt.setString(2, Convertmd(password));
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			int user_id=rs.getInt("user_id");
			return new UserBeans(loginIdData, nameData,user_id);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<UserBeans> findAll() {
		Connection conn = null;
		List<UserBeans> userList = new ArrayList<UserBeans>();
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * from user";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int user_id = rs.getInt("user_id");
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				Date birth_date = rs.getDate("birth_date");
				String password = rs.getString("password");
				Date create_date = rs.getDate("create_date");
				UserBeans user = new UserBeans(user_id, login_id, name, birth_date, password, create_date);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	//プライマリーキーでの呼び出し
	public UserBeans findById(String user_id) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE user_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, user_id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int userid = rs.getInt("user_id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			Date createDate = rs.getDate("create_date");
			UserBeans user = new UserBeans(userid, loginId, name, birthDate, password, createDate);
			return user;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ユーザ情報の追加

	public void insertUser(String login_id, String password, String name, String birth_date) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "insert into user(login_id,password,name,birth_date,create_date)values(?,?,?,?,now())";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, login_id);
			pStmt.setString(2, Convertmd(password));
			pStmt.setString(3, name);
			pStmt.setString(4, birth_date);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}

	}

	//ユーザアップデート
	public void UpdateUser(String password, String name, String birth_date, String user_id) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "update user set password =?,name =?,birth_date =? where user_id =?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, Convertmd(password));
			pStmt.setString(2, name);
			pStmt.setString(3, birth_date);
			pStmt.setString(4, user_id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}

	}

	public void deleteUser(String user_id) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "delete from user where user_id =?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, user_id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//ユーザログインIDの重複探し
//	public User findLoginId(String login_id) {
	public boolean findLoginId(String login_id) {
		Connection conn = null;
		boolean isDuplicatedLoginId = false;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, login_id);
			ResultSet rs = pStmt.executeQuery();

			if (rs.next()) {
				isDuplicatedLoginId = true;
			}

			return isDuplicatedLoginId;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return false;
				}
			}
		}

	}

//	md5化
	public String Convertmd(String password) {

	String source = password;

	//ハッシュ生成前にバイト配列に置き換える際のCharset
	Charset charset = StandardCharsets.UTF_8;
	//ハッシュアルゴリズム
	String algorithm = "MD5";

	//ハッシュ生成処理
	byte[] bytes = null;
	try {
		bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
	} catch (NoSuchAlgorithmException e) {
		e.printStackTrace();
	}
	String result = DatatypeConverter.printHexBinary(bytes);

	return result;
	}


	//ユーザ検索
	public List<UserBeans> search(String login_id, String name, String stBirth_date,String edBirth_date) {
		Connection conn = null;
		List<UserBeans> userList = new ArrayList<UserBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * from user where login_id != 'admin'";

			if(!login_id.equals(""))  {
				sql += " AND login_id like '%" + login_id + "%'";
			}

			if(!name.equals(""))  {
				sql += " and  name like '%" + name + "%'";

			}
			if(!stBirth_date.equals("")) {
				sql += " and birth_date >= '" + stBirth_date +"'";
			}

			if(!edBirth_date.equals("")) {
				sql += " and birth_date <= '" + edBirth_date +"'";
			}
			// SELECTを実行し、結果表を取得

			System.out.println(sql);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("user_id");
				String loginId = rs.getString("login_id");
				String name2 = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				Date createDate = rs.getDate("create_date");
				UserBeans user = new UserBeans(id, loginId, name2, birthDate, password, createDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}
}
