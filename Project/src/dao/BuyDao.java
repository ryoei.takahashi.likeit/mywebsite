package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.BuyBeans;


public class BuyDao {

//	購入履歴の生成
	public void CreateBuyHistory(int user_id,int car_id,int option_id,int total_price){
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "insert into buy(user_id,car_id,option_id,total_price,create_date)values(?,?,?,?,now());";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, user_id);
			pStmt.setInt(2,car_id);
			pStmt.setInt(3,option_id);
			pStmt.setInt(4,total_price);
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}


//	購入履歴　全件検索
	public List<BuyBeans> getBuyHistoryAll() throws SQLException {
		Connection conn = null;
		List<BuyBeans> buyList = new ArrayList<BuyBeans>();
		try {

				conn = DBManager.getConnection();

				String sql = " select * from buy "
						+ "inner join car on buy.car_id = car.car_id "
						+ "inner join user on buy.user_id = user.user_id "
						+ "inner join optionmenu on buy.option_id = optionmenu.option_id;";

				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);

				while (rs.next()) {
					String user_name = rs.getString("user.name");
					String car_name = rs.getString("car.name");
					String menu = rs.getString("menu");
					int price = rs.getInt("car.price");
					int total_price =rs.getInt("total_price");
					int buy_id = rs.getInt("buy_id");
					Date createDate = rs.getDate("create_date");

					BuyBeans buy = new BuyBeans(user_name,car_name,menu,price,total_price,buy_id,createDate);

					buyList.add(buy);
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
			return buyList;
		}
//	購入履歴　ID検索
	public BuyBeans getBuyHistoryById(int buyId) throws SQLException {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = " select * from buy "
					+ "inner join car on buy.car_id = car.car_id "
					+ "inner join user on buy.user_id = user.user_id "
					+ "inner join optionmenu on buy.option_id = optionmenu.option_id where buy_id=?;";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, buyId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			String user_name = rs.getString("user.name");
			String car_name = rs.getString("car.name");
			String car_maker = rs.getString("car.maker");
			String menu = rs.getString("menu");
			int price = rs.getInt("car.price");
			int op_price =rs.getInt("optionmenu.price");
			int total_price =rs.getInt("total_price");
			Date createDate = rs.getDate("create_date");

			BuyBeans history = new BuyBeans(user_name,car_name,car_maker,menu,price,op_price,total_price,createDate);
			return history;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}


//	車の購入時にDetailを売り切れにアップデート
	public void SoldCar(int car_id) throws SQLException {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "update car set detail ='Sold Out!!',update_date = now() where car_id =?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, car_id);
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}

	}
}
